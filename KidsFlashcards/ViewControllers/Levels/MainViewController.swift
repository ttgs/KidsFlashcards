/*
 * Copyright (c) 2018 Razeware LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import UIKit
import StoreKit

class MainViewController: UITableViewController {
  
  // MARK: - Private
  
  private var dataSource: LevelsDataSource!
  
  // MARK: - Public
  
  static let showLevelDetailSegueID = "LevelDetail"
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    dataSource = LevelsDataSource(viewController: self)
    tableView.delegate = dataSource
    tableView.dataSource = dataSource
    
    tableView.register(CustomHeaderView.self, forHeaderFooterViewReuseIdentifier: CustomHeaderView.reuseIdentifer)
    
    refreshControl = UIRefreshControl()
    refreshControl?.addTarget(self, action: #selector(reload), for: .valueChanged)
    
    addRestoreButton()
    registerNotification()
    reload()
  }
  
  @objc func reload() {
    dataSource.storeProducts = []
    tableView.reloadData()
    KidsFlashCardsProducts.store.requestProducts{success, products in
      if success {
        self.dataSource.storeProducts = products!
        self.tableView.reloadData()
      }
      
      self.refreshControl?.endRefreshing()
    }
  }
  
  private func registerNotification() {
    NotificationCenter.default.addObserver(self,
                                           selector: #selector(handlePurchaseNotification(_:)),
                                           name: NSNotification.Name(rawValue: IAPHelper.IAPHelperPurchaseNotification),
                                           object: nil)
  }
  
  private func addRestoreButton() {
    let restoreButton = UIBarButtonItem(title: "Restore",
                                        style: .plain,
                                        target: self,
                                        action: #selector(restoreTapped(_:)))
    navigationItem.rightBarButtonItem = restoreButton
  }
  
  @objc func restoreTapped(_ sender: AnyObject) {
    KidsFlashCardsProducts.store.restorePurchases()
  }
  
  @objc func handlePurchaseNotification(_ notification: Notification) {
    guard let productID = notification.object as? String else { return }
    
    for (index, product) in dataSource.storeProducts.enumerated() {
      guard product.productIdentifier == productID else { continue }
      
      tableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: .fade)
    }
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == MainViewController.showLevelDetailSegueID, let destination = segue.destination as? CardsViewController {
      if let cell = sender as? UITableViewCell, let indexPath = tableView.indexPath(for: cell) {
        destination.product = dataSource.storeProducts[indexPath.row]
      }
    }
  }
  
  override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
    if identifier == MainViewController.showLevelDetailSegueID {
      guard let indexPath = tableView.indexPathForSelectedRow else {
        return false
      }
      
      let product = dataSource.storeProducts[(indexPath as NSIndexPath).row]
      return KidsFlashCardsProducts.store.isProductPurchased(product.productIdentifier)
    }
    
    return true
  }
}
