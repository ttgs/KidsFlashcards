/*
 * Copyright (c) 2018 Razeware LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import UIKit

class CustomHeaderView: UITableViewHeaderFooterView {
  
  static let reuseIdentifer = "CustomHeaderReuseIdentifier"
  let customLabel = UILabel()
  
  override public init(reuseIdentifier: String?) {
    super.init(reuseIdentifier: reuseIdentifier)
    
    customLabel.font = UIFont.preferredFont(forTextStyle: .largeTitle)
    
    customLabel.translatesAutoresizingMaskIntoConstraints = false
    self.contentView.addSubview(customLabel)
    
    let margins = contentView.layoutMarginsGuide
    NSLayoutConstraint.activate([
      customLabel.leadingAnchor.constraint(equalTo: margins.leadingAnchor),
      customLabel.trailingAnchor.constraint(equalTo: margins.trailingAnchor),
      customLabel.topAnchor.constraint(equalTo: margins.topAnchor),
      customLabel.bottomAnchor.constraint(equalTo: margins.bottomAnchor)
      ])
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}
