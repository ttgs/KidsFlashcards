/*
 * Copyright (c) 2018 Razeware LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import Foundation
import StoreKit

/// Levels IDs
public enum Level: String {
  case level1 = "RWLevel1"
  case level2 = "RWLevel2"
  case level3 = "RWLevel3"
  
  var productId: String {
    return (Bundle.main.bundleIdentifier ?? "") + "." + rawValue
  }
  
  init?(productId: String) {
    guard let bundleID = Bundle.main.bundleIdentifier, productId.hasPrefix(bundleID) else {
        return nil
    }
    self.init(rawValue: productId.replacingOccurrences(of: bundleID + ".", with: ""))
  }
}

public struct KidsFlashCardsProducts {
  
  public static let products = [Level.level1,
                                Level.level2,
                                Level.level3]
  
  public static let freeProducts = [Level.level1]
  
  fileprivate static let productIdentifiers: Set<ProductIdentifier> = Set(products.map { return $0.productId })
  fileprivate static let freeProductIdentifiers: Set<ProductIdentifier> = Set(freeProducts.map { return $0.productId })
  
  public static let store = IAPHelper(productIds: KidsFlashCardsProducts.productIdentifiers,
                                      freeProductIds: KidsFlashCardsProducts.freeProductIdentifiers)
}

func resourceNameForProductIdentifier(_ productIdentifier: String) -> String? {
  return productIdentifier.components(separatedBy: ".").last
}
