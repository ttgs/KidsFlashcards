/*
 * Copyright (c) 2018 Razeware LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import UIKit

struct CardData {
  let imageName: String
  let message: String
}

class CardsDataSource: NSObject, UIPageViewControllerDataSource, UIPageViewControllerDelegate {
  
  // MARK: - Private
  
  private let storyboard = UIStoryboard(name: "Main", bundle: nil)
  private let individualCardViewControllerID = String(describing: IndividualCardViewController.self)
  
  // MARK: - Public
  
  static let freeProductID = Level.level1.productId
  var currentLevelID = CardsDataSource.freeProductID
  
  /// Levels content
  let data: [String: [CardData]] = [
    Level.level1.productId: [CardData(imageName: "card1", message: "An Arm and a Leg"),
                             CardData(imageName: "card2", message: "Back to Square One"),
                             CardData(imageName: "card3", message: "Beating Around the Bush"),
                             CardData(imageName: "card4", message: "Beating a Dead Horse"),
                             CardData(imageName: "card5", message: "A Dime a Dozen")],
    Level.level2.productId: [CardData(imageName: "rwdevcon-bg", message: "Close But No Cigar"),
                             CardData(imageName: "rwdevcon-bg", message: "Cry Wolf"),
                             CardData(imageName: "rwdevcon-bg", message: "Curiosity Killed The Cat"),
                             CardData(imageName: "rwdevcon-bg", message: "Cut To The Chase"),
                             CardData(imageName: "rwdevcon-bg", message: "Cry Over Spilled Milk")],
    Level.level3.productId: [CardData(imageName: "rwdevcon-bg", message: "Cry Wolf"),
                             CardData(imageName: "rwdevcon-bg", message: "Fish Out Of Water"),
                             CardData(imageName: "rwdevcon-bg", message: "Foaming At The Mouth"),
                             CardData(imageName: "rwdevcon-bg", message: "Off Base"),
                             CardData(imageName: "rwdevcon-bg", message: "Out Of Left Field")]
  ]
  
  // MARK: - Presenting page controller
  
  func presentationCount(for pageViewController: UIPageViewController) -> Int {
    return data[currentLevelID]?.count ?? 0
  }
  
  func presentationIndex(for pageViewController: UIPageViewController) -> Int {
    guard let page = pageViewController.viewControllers?.first as? IndividualCardViewController else {
      return 1
    }
    return page.pageIndex + 1
  }
  
  // MARK: - Presenting page
  
  func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
    
    guard let individualCardViewController = viewController as? IndividualCardViewController,
      let data = data[currentLevelID] else {
      return nil
    }
    
    let newCardViewController = createCardPage()
    let previousIndex = individualCardViewController.pageIndex - 1
    newCardViewController.pageIndex = previousIndex < 0 ? data.count - 1 : previousIndex
    newCardViewController.pageData = data[newCardViewController.pageIndex]
    return newCardViewController
  }
  
  func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
    
    guard let individualCardViewController = viewController as? IndividualCardViewController,
      let data = data[currentLevelID] else {
      return nil
    }
    
    let newCardViewController = createCardPage()
    let nextIndex = individualCardViewController.pageIndex + 1
    newCardViewController.pageIndex = nextIndex < data.count ? nextIndex : 0
    newCardViewController.pageData = data[newCardViewController.pageIndex]
    return newCardViewController
  }
  
  // MARK: - Helpers
  
  func getCardData(_ pageNumber: Int) -> CardData? {
    return data[currentLevelID]?[pageNumber]
  }
  
  func createCardPage() -> IndividualCardViewController {
    return storyboard.instantiateViewController(withIdentifier: individualCardViewControllerID) as! IndividualCardViewController
  }
}
