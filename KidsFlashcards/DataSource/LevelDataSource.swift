/*
 * Copyright (c) 2018 Razeware LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import UIKit
import StoreKit

// MARK: - UITableViewDataSource

class LevelsDataSource: NSObject, UITableViewDataSource {
  
  private static let reuseIdentifer = "titleBasicCell"
  private let viewController: MainViewController
  
  // MARK: - Public
  
  var storeProducts: [SKProduct] = []
  
  init(viewController: MainViewController) {
    self.viewController = viewController
  }
  
  // MARK: - UITableViewDataSource
  
  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return storeProducts.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: LevelsDataSource.reuseIdentifer) as! LevelCell
    
    cell.product = storeProducts[indexPath.row]
    cell.buyButtonHandler = { product in
      KidsFlashCardsProducts.store.buyProduct(product)
    }
    
    return cell
  }
  
  
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    let sender = tableView.cellForRow(at: indexPath)
    if viewController.shouldPerformSegue(withIdentifier: MainViewController.showLevelDetailSegueID, sender: sender) {
      viewController.performSegue(withIdentifier: MainViewController.showLevelDetailSegueID, sender: sender)
    }
  }
  
  func productLevel(row: Int) -> SKProduct {
    return storeProducts[row]
  }
}

// MARK: - UITableViewDelegate

extension LevelsDataSource: UITableViewDelegate {
  
  func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: CustomHeaderView.reuseIdentifer) as? CustomHeaderView
    headerView?.customLabel.text = "Choose a level"
    headerView?.contentView.backgroundColor = UIColor.white
    return headerView
  }
  
  func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    return 50
  }
}
